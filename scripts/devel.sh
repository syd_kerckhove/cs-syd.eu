#!/bin/bash

set -e
set -x

stack install :gio --file-watch --exec=scripts/redo-gio.sh --ghc-options=-freverse-errors
