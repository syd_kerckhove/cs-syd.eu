---
layout: page
title: Talks
published: 2015-07-04
last-updated: 2015-07-04
tags: notes, math
---

These are the slides for talks I've given.

<div></div><!--more-->

As you can see, I haven't given many talks.
That's something I would very much like to change so if you would like me to speak at your event, please contact me.

- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ Google Zürich 2017-12-08 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md)
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ HaskellerZ 2017-11-30 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ Haskell Exchange 2017-10-12 [PDF](/assets/easyspec/public-presentation.pdf) [Video](https://skillsmatter.com/skillscasts/10629-signature-inference-for-functional-property-discovery)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ HaskellerZ 2017-07-27 [PDF](/assets/easyspec/public-presentation.pdf) [Video](https://www.youtube.com/watch?v=CIyJ4q205iM)
- [Super User Spark: A safe way to never worry about your beautifully configured system again](https://syd_kerckhove@bitbucket.org/syd_kerckhove/sus-talk.git) @ CCC-ZH 2016-04-20
- [An introduction to Haskell](https://bitbucket.org/syd_kerckhove/ccczh-intro-to-haskell) @ CCC-ZH - 2016-01-16
- [Artificiële Intelligentie, een Introductie (dutch)](https://bitbucket.org/syd_kerckhove/marnix-ai/overview) @ Marnix Ring - 2015-04-22
