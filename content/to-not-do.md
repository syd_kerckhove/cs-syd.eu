---
title: To-not-do list
---

This list is the dual of a conventional to-do list.
It is a list of things that I have committed to not doing.

- Watch Television
- Watch Television Series
- Be worldly
- Know a lot about music
- Read fiction
- Play computer games
- Building electrical circuits
- Solve [Rosalind](http://rosalind.info) problems
- Solve [Project Euler](https://projecteuler.net/) problems
- Play [Over the wire wargames](http://overthewire.org/wargames/)
- Create tiny appliances controlled by an Arduino micro controller
- Learn Clojure
- Learn Erlang
- Play coding games
- Enter programming competitions
- Configure, compile and use a custom linux kernel
- Create an operating system
- Create a filesystem
- [Take the Eudyptula Challenge](http://eudyptula-challenge.org/)
- [Take the crypto challenges](http://cryptopals.com/)
- Write a connect-4 solver
