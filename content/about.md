---
layout: page
title: About
header: About
---

Hi! My name is Tom Sydney Kerckhove.

I am interested in software that works.
In particular, I focus on (property) testing and devops.
See ["What I can do for your company"](/services.html)
