---
layout: quote
author: Anita Shreve
quote: If you're skating on thin ice, you might as well dance.
title: If you're skating on thin ice, you might as well dance. - Anita Shreve
tags: fun, risk
---

If you're in trouble anyway, you might as well have fun!
Life has its ways of getting you into a bigger mess than you could imagine.
If you don't, somehow, find a way to enjoy that, your life will be miserable anyway.
You can, of course, enjoy this part of life, but it will require some effort.

If you're currently down, I challenge you to take another risk.
If it works out, you're already on your way back up.
If it doesn't, you'll probably have a smile on your face, and a crazy story to tell.
Hopefully you will enjoy yourself in any case.
