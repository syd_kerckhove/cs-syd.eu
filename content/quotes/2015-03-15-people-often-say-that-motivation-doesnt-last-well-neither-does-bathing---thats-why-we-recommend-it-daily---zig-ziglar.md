---
layout: quote
author: Zig Ziglar
quote: People often say that motivation doesn't last. Well, neither does bathing - that's why we recommend it daily.
title: People often say that motivation doesn't last. Well, neither does bathing - that's why we recommend it daily. - Zig Ziglar 
tags: motivation, hygiene
---

It surprises me greatly how many people are unmotivated.
I have heard things like "that's too hard", "why would I?" and "but, that takes effort!" countless numbers of times.
Why is it that we take motivation less serious than personal hygiene?

Just like hygiene, taking care of your personal motivation is your own responsibility.
It is best to make it a habit that you can take care of yourself, but motivation through others is better than none.

The first thing to do is, as always, write down your goals.
Don't just think of them while daydreaming.
Writing your goals down makes you take responsibility for realising them.

Suppose you have a perfectly organized, hierarchical schema of all your life's goals sorted by estimated due date.
It's possible, although unlikely, that you're still not motivated to do anything.
Let me take this opportunity to tell you that it's okay to seek help to motivate yourself.
This is especially true if you've never had the habit of taking care of your motivation.

It's okay to listen to motivational sound tracks.
It's good to look at the people who are where you want to be.
It's fine to have people help motivate you,
However, ideally, you want to be able to motivate yourself just like you like to be able to wash yourself.

I argue that motivation is as important as personal hygiene, but that the habit is less easily taught.
I'll show you [what got me started](https://www.youtube.com/watch?v=5fsm-QbN9r8).
