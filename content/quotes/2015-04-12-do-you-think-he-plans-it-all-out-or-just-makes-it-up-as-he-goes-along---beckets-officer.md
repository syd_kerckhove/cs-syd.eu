---
layout: quote
quote: Do you think he plans it all out, or just makes it up as he goes along?
author: Becket's Officer
title: Do you think he plans it all out, or just makes it up as he goes along? - Becket's Officer
tags: pirate, plan, compass, direction, goals
---

So, what do you think?
Does Captain Jack Sparrow plan everything out?

I think he doesn't need to plan a lot out.
He has this compass that points to the thing he wants most at all times.
Anyone who knows me knows that my advice on anything starts with "What do you want?".
Imagine of what kind of value that compass could be.
If you know what you want, or you know where to go to get it, you can make everything else up as you go.

So, where are *you* going?

