---
title: What I can do for you 
---

Alternative title: How you can make money off of hiring me.

I do freelancing work as a software engineer.
I focus on improving the following aspects of software systems.

* Robustness

We expect software to work but software still misbehaves a lot.
I am passionate about systematic improvements to the robustness of systems.

* Ease of maintenance

[Maintenance accounts for more than half of the cost of the software engineering process](https://www.tutorialspoint.com/software_engineering/software_maintenance_overview.htm).
Optimising the ease of maintenance can have a significant impact on the cost of maintaining the system.

* Automation

Removing human involvement in a system is a great way to increase reliability and decrease costs.
This is why we build software in the first place.
When it comes to maintenance, however, automation is often overlooked.

### Specifically

#### Software

* [Hire me to see if pure functional programming is something for your company](/contact.html)
* [Hire me to improve your testing situation](/contact.html)
* [Hire me to improve your deployment situation](/contact.html)
* [Hire me to find safety-holes in your software](/contact.html)
* [Hire me to teach a class](/contact.html)
* [Hire me to write technical documentation](/contact.html)
* [Hire me to write a blogpost](/contact.html)
* [Hire me to give a talk](/contact.html)

#### Coaching

* [Personal management](/contact.html)
* [Personal productivity](/contact.html)
* [Self-moderation](/contact.html)
* [Programming](/contact.html)
