---
title: Thesis
---

- [EasySpec Github Repository](https://github.com/NorfairKing/easyspec)
- [Thesis Github Repository](https://github.com/NorfairKing/thesis)
- [Thesis PDF](https://github.com/NorfairKing/thesis/raw/master/pdfs/thesis-tom-sydney-kerckhove.pdf)
- [Public Talk Slides](/assets/easyspec/public-presentation.pdf)
- [Public Talk video](https://www.youtube.com/watch?v=CIyJ4q205iM)
- [Academic Talk Slides](/assets/easyspec/academic-presentation.pdf)
- [Public Talk video](https://skillsmatter.com/skillscasts/10629-signature-inference-for-functional-property-discovery)
