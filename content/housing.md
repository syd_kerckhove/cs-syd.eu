---
title: Room for rent in a shared apartment.
---

## Offer

* Size: 100 m^2
* Rooms: 4.5
* Furniture: Common areas are furnished, furniture in the rooms is to be negotiated with the current tenants
* Free from: 2017-07-01
* Period: Indefinite.
* Smoking: Not allowed inside but there is a balcony.
* Deposit: One month rent.
* Price: 800 CHF per month per room (all-in)
* Notice: One month

* Address: Altwiesenstrasse (For more details see below.)
* Public Transport Connections: Single tram to center (7) and Single tram to University (9).
* Time to ETH: 25 min
* Time to Center: 25 min
* Time to Hönggerberg: 30 min

* Dishwasher
* Gigabit internet connection
* Shared Washing room and drying room
* Bike Garage
* Storage room in the cellar and in the attic
* Bathrooms: 2 for 3 people
* Room size: 11 m^2

## Viewings

To organize a viewing, please send me an email.
<a href=/contact.html>Click here to find my contact details.</a>
Please suggest a time on a weekend and tell me a bit about yourself in this email as well.

For more information about me, look on this site.
 
You will get the full address when making an appointment.

## Pictures

![](/assets/housing/1.jpg)
![](/assets/housing/2.jpg)
![](/assets/housing/3.jpg)
![](/assets/housing/4.jpg)
![](/assets/housing/5.jpg)
![](/assets/housing/6.jpg)
![](/assets/housing/7.jpg)
![](/assets/housing/8.jpg)
