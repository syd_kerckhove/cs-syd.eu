---
title: Bounty hunting
---

A bounty consists of a piece of work that I would like to see done.  A bounty
can be claimed so that no one else will start working on it in the meantime.
When you complete the work, you earn the bounty.  If you would like to become a
bounty hunter, please contact me to go over the details.

Hunting is open to anyone who would like to practice (Haskell) programming.
Bounty hunters will be mentored and their work reviewed.
This is your chance to make money while learning Haskell.

### Topics

Most of the work is open-source and it is mostly Haskell programming.
There is work suitable for a variety of experience with Haskell.
There is work on different topics as well:

- Command-line tools
- Front-end (Reflex, GHCJS)
- Back-end
- Web

### Open bounties

#### [Zifter](https://github.com/NorfairKing/zifter)

- Output serialisations

Zifter is built on the idea that you should be able to parallelise validation of
a repository, but still get the output as if the validation was done
sequentially. There is an under-approximation of this idea, but now I want it
done properly.

- Fix the hindent version on a per-commit basis
  
Hindent changes a lot, and on a per-project basis, the user should be able
to stick to a specific hindent version.

#### [Validity](https://github.com/NorfairKing/validity)

- Validity path generators.

`genvvalidity-path` has generators for paths, but they are rather slow.
I would like some faster general purpose generators, and also more specific generators.

#### [Genfile](https://github.com/NorfairKing/genfile)

- Get the project ready for public use. This involves: a zift script, a travis
  setup using that zift script and fixing the dependency bounds.

#### [Smos](https://github.com/NorfairKing/smos)

- Org-mode to smos converter

Smos is intended as a replacement for org-mode, and it is not compatible with
the org mode format. It would be nice if one could just convert their org mode
documents instead of starting over with smos.
We do not need a converter in the other direction.

#### [Hastory](https://github.com/NorfairKing/hastory)

- Automatic alias suggestions

Hastory already collects a lot of information. The next step is to be able to
suggest new shell aliases that may be useful to the user. Ideally there should
be a way to quantify how useful it would be. This should not only work within
the single command boundary, but also across multiple commands.

- Bug: working directory is gathered after commands are run

The 'current working directory' is gathered after a command is run, instead of
before, which means that it is wrong in the case of `cd`.

#### Other bounties

There are some other bounties available on projets that are not yet open-source:

- An API-first, CLI second, GUI last Intray (cf GTD)
- [A command-line habit tracker](/projects/suggestions/habitscipline)

### Claimed bounties

#### [Super User Spark](https://github.com/NorfairKing/super-user-spark)

- [2018-01-15] SUS: bug: non-injective homotopy warning
- [2018-01-15] SUS: bug: deployment is possible, even though it is not.

### Completed bounties

- Intray: authentication
- Workflow: next actions
- Validity: genvalidity-hspec-hashable
