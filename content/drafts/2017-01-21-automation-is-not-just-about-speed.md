---
title: Automation is not just about speed
tags: automation, speed, safety
---

I recently got a chance to to approach an entirely unexplored (by myself) problem.
During a number of similar discussions, I learned a few things.

### The problem

This semester I took a course called 'Advanced Systems Lab'.
The course involved a big project that involved 

- Building a middleware for Memcache
- Setting up a couple of servers on Microsoft Azure
- Running many experiments on those servers involving multiple servers in each experiment
- Collecting and malipulating the results
- Presenting the results and interpreting them
- Writing a report

The number of servers was of the order 1E2, the number of experience was of the order of 1E3, the length of the experiments was of the order of minutes.

### An alternative approach

Most students wrote shell scripts, Python scripts and even spreadsheets.
They would write a tiny script, ssh into each of the servers and execute the scripts.

I started out this way as well, but quickly saw that this approach would quickly kill off any motivation I had for this course.
Both for the sake of my mental state and the quality of the work, I decided that this approach just was not good enough.

I approached the entire system as a big build system.
I need to submit a support, which means I need the text and plots, which means I need results and plotting scripts, which means I need to run the experiments, ... etc.
Each part of the process had explicitly defined dependencies and build rules.

### The discussion

- People kept asking me: How much time have you saved with this automation?
Sure, it would have cost me less time to do it correctly once, but doing things correctly is not what costs time.
If you take into account that I make mistakes (a lot), doing things manually would have cost me about four times more time.
More importantly: automating everything is so much more fun!

- It's also about safety
- reducing errors
- Forces you to think the process through
- Allows you to think bigger as you are enabled to compose the current automation
- Don't be afraid to modify automation.
- It's all too easy to say "oh I'll just do this manually once!".
