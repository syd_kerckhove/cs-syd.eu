---
layout: post
title: "Jobhunt: Standard Chartered"
tags: jobhunt
category: Stories
---

Standard chartered has been on my 'companies to apply to' list for as long as I have been writing Haskell.
I applied with them, but things did not turn out as well as I had hoped.

<div></div><!--more-->

### Application stage

I first contacted SC in February, after they posted on Reddit that they were hiring.
At that time, I was told that September was too far away to interview me already.

I applied again in April, when another Reddit post showed up, this time by another person.
A few days later I was invited for a phone screening where I was asked basic Haskell questions.
So far so good.

### On-site interviews

Next, I was invited to be interviewed on-site in London.
The flights were paid for, but there was a miscommunication about the airports that extended my commute time by a total of four hours.
I guess I can't really complain about free flights, and the company paid for transport from and to the airport.

The day of interviews was a lot of fun, the questions were remarkably useful with respect to actual software engineering.
There were no algorithm brain-teasers, only real-world-applicable questions.
I got to have lunch with two members of the team, and got to meet almost every person on the team in London.

I told the team that I would not be available until after I graduated, in September.

After the on-site interviews, I sent thank-you emails and asked for feedback.
It took a week for anyone to respond, and I was told to wait two more days.
HR would reach out to me.

### Offer stage

After another week, I finally got an email from HR.
It was an offer sketch from `David` from the operations team:

```
Rate – XXX per day (3/4 my current rate, as a student)
Duration – 1 year
Notice – 1 month
Start Date – TBC (ASAP)
```

I was also asked for these pieces of information:

```
If you could please confirm your acceptance and pass across the below details , we can look to get the onboarding process started.
- Full name
- Date of Birth
- Nationality
- Telephone number
- Ltd Company
- Email
- Earliest Start Date 
```

You read that right. They asked me for my `Email` _via email_.
Note that I'd already given them most of this information.
I was asked to `confirm acceptance`, without even receiving a contract, so I said:

```
I would definitely be interested.
Of course I cannot accept formally until I get a full offer.
```

... and filled out the information they asked for, again.
I also asked what they meant by `Ltd Company` and explained my situation in Switzerland.

### Confusion

Next, I got an email from `Simon` the operations manager.
He explained the details of the `Ltd Company` question to me, and asked me again to `confirm you are happy to accept`.
I still had not seen a contract, so I replied:

```
This looks good to me, but whether I accept is going to depend on the
exact offer, not on this data. I need to see all relevant documents
before I can accept.
```

The next reply I got mentioned a start date ...

```
I would propose a target start date of Monday 3rd July (subject to screening).
```

(Note that I'd already told the team that I would not be available before September.)
... and at the end of the email, it said:


```
Jon - please can you begin the on boarding process.
```

(`Jon` was cc-ed.)

### Documents

Before I could respond about the start date, I got a barrage of emails from a few different people with:

- Background Verification from `Allegis Global Solutions` with a username.
- An email with the username and password for the background verification (in plaintext)
- A reminder that I should fill out the background verification
- An email that gave me the username and password for the paperwork from `Kayleigh`, a `Customer Support Associate` (in plaintext)
- An `updated employee statement` document
- Another reminder that I should fill out the background verification

#### Background verification

As part of the on boarding I would have to undergo a screening process.
The email that introduced me to this process included the following instructions:

```
To start the process please log onto https://cspi.fadv.com/cde/  using the user name listed below and complete the required information as directed on screen.
Please ensure that you visit the web address listed above and complete the required information within the next 24 hours in order to avoid delay to the screening process and your start date.
```

The email then proceeded to enumerate which info I would have to upload:

```
Please also upload a copy of the following documentation:
- Passport (and visa, if applicable)
- Proof of address (eg. utility bill)
- Certificate for highest level of education
- Certificate for Professional Qualification (if applicable)
- Employment History (minimum 2)
  - Pay Slips
  - Reference Letter
  - Release Letter
  - Employment Tax Documentation
  - Offer Letter
  - Employment Contract (Contract  of Service)
  - Performance, Benefit or Reward Documentation
  - Enrolment Documentation or Benefit Statements relating to Pension Scheme
```

... and if that wasn't scary yet:

```
(Please note that the screening tool is best viewed using Internet Explorer 7 or higher.)
```

I did not start this process, because I decided to read through the contract first.

### Questions

I never got the chance to read the contract in its entirity, but I asked some questions about the parts that I had read:

- The rate.
- The confidentiality agreement had no end-date.
- There was no mention of working hours.
- There was an opt-out on the topic of working time regulations.

I concluded the email by articulating my concerns about the proposed start date:

```
I cannot start on Monday July 3rd.
For full-time onsite work, I am only available from the end of September.
```

Following my questions, I got a strongly worded email from `Jon`:

```
Initially, I would like to highlight the fact that you read and agreed, repeatedly on emails, the required start date (knowing it was full time in the UK) and also the rate of £XXX per day. I am unsure where it has only come to your surprise when you have received post-confirmed and agreed offer, that you are challenging this?
```

My questions were also partly answered.
Not a single letter of the contract was up for negotation.


### Rejection

In the next email, I clarified that I had never actually agreed to anything, nor confirmed any start date.
I got `apologies for unfortunate circumstances/communications.` and was asked `Are we in agreement that we should cease this process now?`.
I told them that I was still interested in the position, but not available before September, and asked them how we could proceed.

The apology was quickly undone when they replied:

```
As mentioned below, the timelines by which we presented the offer and you accepted, are the ones you will need to adhere to.
```

At this point I had had enough and ended the chain of emails with:

```
At this point I believe that we will not be able to work together at this time.
Thank you for your patience.
```

I sent a last email to the team, thanking them for their time, and telling them `HR has contacted me, but we have not been able to come to an agreement.`.

### Reddit

Some time later, a [reddit post appeared](https://www.reddit.com/r/haskell/comments/6im268/standard_chartered_is_hiring_again/), that said Standard Chartered was still hiring.
I commented that I would not recommend applying, and got enough votes to make me think that my story is not an exception.

### Timeline:

- 2017-02-12: First contact
- 2017-02-23: Initial rejection
- 2017-04-29: Second application
- 2017-05-02: First response to second application
- 2017-05-08: First phone screening
- 2017-05-16: Five on-site interviews
- 2017-05-23: Response after interviews
- 2017-05-30: Offer sketch
- 2017-06-13: Offer rejection
- 2017-06-21: Reddit post
