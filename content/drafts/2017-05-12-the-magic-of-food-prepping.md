---
layout: post
title: The magic of food prepping
category: Magic
tags: food, productivity, cooking
---

I had always wanted to try food prepping, but only recently have I actually started doing it.
It turns out that the last barrier was to buy a dozen containers to store food.

<div></div><!--more-->

### The Problem

The biggest problem I have with eating well, is that it is a lot easier to not eat well.
Ordering pizza and having it delivered 25 minutes later is a lot easier than going to a store, buying food, cooking that food, and washing up afterward.
On top of that, the taste of pizza makes it all-too-easy for my subconcious to convince the part of me that wants to eat well that pizza is a good idea.

### Benefits of food propping

### Cons of food prepping

Cooking once per week, eating the rest of the week.

Pros:
- Very litle time is spent cooking, overall. It takes only marginally longer to cook a lot of food than it does to cook a single meal.
- It's a lot easier to get whatever you need because you only need to shop for that one cooking session.
- As a result you can put together a very nutricious meal.
- You will create a situation in which it is easier to eat well than to order pizza. [That ties in with making the best option the easiest one.](http://matt.might.net/articles/least-resistance-weight-loss/)

Cons:
- Eating the same thing multiple times; is not a problem for me

