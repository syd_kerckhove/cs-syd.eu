---
layout: page
title: Customer Service From Hell
state: Idea
published: 2017-05-12
last-updated: 2017-05-12
tags: Customer Service, website
---

I have been reading [clients from hell](http://clientsfromhell.net) for a long time now.
It has entertained me to read that I am not the only one who has to deal with annoying clients and that mine are not even that bad.
On the other hand, I am also an unapologetic complainer when it comes to customer service.
The idea for this project is a combination of those ideas.

<div></div><!--more-->

### The idea

The idea is to build a simple web site where anyone can share stories about terrible customer service.
Making these customer service failures public should incentivise companies to do a better job.

A place to complain should serve to allow people to vent about bad service, and to realise that their situation could be much worse.

A place like clientsfromhell.com where people can complain about customer service
The idea is that people get identifying information about the persons that helped them on the company end, so that they can be held accountable at the company end.

