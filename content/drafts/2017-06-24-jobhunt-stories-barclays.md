---
layout: post
title: "Jobhunt: Barclays"
tags: jobhunt
category: Stories
---

Barclays has started using Haskell with Neil Mitchell at the wheel.
I applied with them as well, but the situation went south for reasons that make me sad.

<div></div><!--more-->

### Application stage

I got invited to apply at Barclays by `Steven Howden` who introduced himself as `Steve`, a headhunter from `TEKSystems`.
I told `Steve` that I was very interested and asked him to proceed with arranging an interview for me.

`Steve` told me (28 april):

```
The only thing stalling the process is the business side having to go through extra approvals to bring on someone with your level of experience, we should have an update early next week.
```

After asking more about this, he told me:

```
Neil is keen to interview you after reviewing your profile but we have to wait for HR at Barclays to approve the hiring for a junior level candidate, unfortunately this has not been helped by several people in HR being out of office the past week hence why it is taking a bit longer for sign off.
```

Much later (June 8th), certainly not `early next week`, I finally got another email from `Steve` saying:

```
Everything has been given the green light to proceed with the sign off for the position complete by HR, this means we can look to proceed with interviews. I will put you through the system officially so we can look to get first round interviews set up accordingly.

Another thing, Neil Mitchell has asked the following:

"Is Tom at the Zurich Hackathon this weekend? I will be, so if he is, tell him to come say hello."
```

At Zurihack, I met Neil Mitchell and went through an initial technical screening and was told that I would be contacted to come on-site for further interviews.

### On-site interviews

I had planned a trip to London the next week anyway, so the following tuesday I went to Barclays' office for on-site interviews.
I had four interviews, every one of them was well thought-out.
The interviewers had done their homework, they knew what they were talking about, and there were no nonsense interview questions.
Kudos to that team!

### Offer stage

#### Data sheet

In the week that followed, I got another email from 'Steve'.
He said:

```
There are a number of details that Neil needs from you for HR reasons, please ensure they are accurate as proof may be required with background checks, I have detailed theses below:

Current salary (breakdown if possible in the currency you are paid in):
Salary at Facebook:
Availability to start (can be adjusted):
Expected starting salary:
```

I know that this request for my previous salary had only one purpose: To reduce my power in negotiation.
I responded to this query saying `That's very sensitive information. Could you tell me more about why they need this?`

I quickly got a reply saying:

```
I have attached a document required by Barclays to complete, please try and fill out as much as possible as accurately as possible.

Such information is required by Human Resources not just at Barclays but all companies for several reasons.

1) Background checks (you are who you say you are and there is proof you earn what you say you earn)
2) Competitive offers on par with the market
3) Data collection for areas to assess salary bandings
4) Stability within an organisation

[...]

I understand it is sensitive information, but at this stage it is necessary information to proceed with putting out an offer.
``` 

I count at least two lies: `Such information is required by Human Resources not just at Barclays but all companies` and `at this stage it is necessary information to proceed`.
For reference, this document can be found [here](/assets/jobhunt-stories-barclays/compensation-questionnaire.pdf).

Now, I would like to tell you that I rejected the offer right here, because of this ridiculous demand.
However, I knew that filling in this document would play in my favour, given my relatively decent compensation history.

I did not respond for a few days, and before I could, I got another email mentioning the following:

```
We cannot proceed with Barclays without you completing the attached form
```

In following compensation, I told `Steve` that I was reluctant to fill in this data sheet, and that I was uncomfortable with sending over any personal information via an insecure email.
I also told him that I could not provide an expected salary, especially without seeing the terms of the contract.

In the next email from Steve, he sounded a bit annoyed:

```
[...] there is absolutely no possibility of you receiving a soft or hard copy of a contract with no commitment from your side to provide any information on your current salary or a ball park figure of a salary you would be happy with.

I am sure you can see that there is no negotiation here. [...]

Without any input from you, we cannot proceed. This is a standard process with any established company in the UK, it will be different with a small start-up with no official process.

To establish a middle ground, I purpose this - I will put it forward as £50-60k base salary as an expectation, as this was a figure put out by HR as the max that can be signed off. [...]
```

I was already annoyed that `Steve` had decided on _my_ expected salary for me, but I filled out the data sheet and sent it over to steve anyway.
He told me Barclays would draw up a contract in the next week.
(Needless to say, it took three weeks until I got a contract sent to me.)

#### Identity

At some point in the week after sending over the data sheet, `Steve` called me about an identity check, and sent me the following email:

```
Please see the attached documents in PDF format detailing what you need to take on site at Barclays, with information on what is acceptable and what is not. You will need to get scanned copies for both with signatures and a Barclays representatives I.D number.

For example I have detailed the steps below:

1) You will need to take your passport and a utility bill that has your name and address to a Barclays branch
2) Mention you have an offer to join Barclays Capital and need a staff member to complete your ID&V checks.
3) Both items will be scanned and printed on A4 paper
4) You AND the Barclays employee will need to add the following to BOTH (if separate pieces of paper):
- Barclays employee name AND your name, PRINTED
- Barclays employee AND your signature
- Date the document was certified
- Barclays employee staff number
5) Both copies with the Barclays employees and your details need to be scanned again and sent to [...]

[...], please do not expect the staff member to automatically know what is needed, [...]

[...]

The contract cannot be drawn up until this has all be completed by you, the sooner this is done the sooner you will have all the details you need. Can you let me know when you will be doing this [...]

Thanks,
Steve
```

For reference, the documents that `Steve` is referring to can be found [here](/assets/jobhunt-stories-barclays/legal-residency.pdf) and [here](/assets/jobhunt-stories-barclays/legal-rights.pdf).

`Steve` started to sound a lot like a scammer, I felt.
I did not appreciate the further lie: `The contract cannot be drawn up until this has all be completed by you`, and was unpleasantly surprised by the negligence captured by the sentence `please do not expect the staff member to automatically know what is needed`.

I really should have protested more about them asking for a copy of my ID and signature, but at this point I just went along with the process as if it was normal or acceptable.
It was not normal nor acceptable.

#### Contract

After that, I got an email with the subject line "Welcome to Barclays!" (as if I was going to accept any offer).
I got a username for the 'Barclays Welcome Portal'.
It did not mention a password in plaintext. I was impressed.

I told `Steve` that I would need some time to read through the documents, because I had an upcoming deadline.
`Steve` tried to rush me again:

```
I understand that you have several priorities, but I think a career in London working with Neil should be pretty high up that list! It also sets a good impression.
```

The contract was split into two documents: [A summary of the terms](/assets/jobhunt-stories-barclays/summary.pdf) and [A detailed account of the terms](/assets/jobhunt-stories-barclays/detailed-terms.pdf)
Obviously the summary document seemed reasonable.
In the detailed terms however, I found these:


- I would have to sign over all my current and future Intellectual property:

```
By signing your Agreement you agree to assign to the Company or a member of the Barclays
Group (as specified by the Company) all current and future Intellectual Property Rights in the
Works and any other proprietary rights capable of assignment by way of present assignment of
future rights for the full term of such rights.
```

- Barclays could arbitrarily change the terms of the contract post hoc:

```
The Company reserves the right to review, revise, amend, replace or withdraw the contents of
the Agreement and introduce new policies, practices, guidelines and procedures from time to
time to reflect the changing needs of the business or any changes in legislation or regulation
from time to time. 
```

- These terms extend beyond the end of the contract:

```
The termination of your employment will not affect any terms of your Agreement which are
intended to operate after the termination date, including the paragraphs relating to Intellectual
Property, Confidentiality and Post Employment Restrictions.
```

I told Steve about my concerns involving these parts of the contract, but I admitted that I am not a lawyer.
I was happy to be explained what these terms actually meant if I had misunderstood them.

`Steve` offered to have Barclays' HR and Neil himself explain to me some parts of the contract.
A few days later I had a call with HR, who esentially told me that they would have to ask legal.
I also called Neil, who explained to me how the terms are actually enforced.

Neither of these were very comforting, given that the explanations came from participants in the contract.
HR also told me `We will not be able to change any of the terms, and the compensation is non-negotiable`.

The first part is a blatant lie, and about the second part of that sentence:
There is no such thing as 'non-negotiable', there is only 'refusing to negotiate'.

#### Compensation

With the terms of the contract cleared up, I was still not happy with the proposed compensation.
The reason for this was that I had already gotten better offers, and had already made more in past employment.

After the calls with HR and with Neil, I got an email from `Steve` that said:

```
Are you in a position from which you will be accepting this position pending the issues/conditions discussed with Neil are all met?
```

I told `Steve`:

```
I'm still waiting on some clarifications on the contract from HR.

> Are you in a position from which you will be accepting this position pending the issues/conditions discussed with Neil are all met?

That's going to depend on the clarifications.
I'm still on the fence with regards to the compensation.
```

Not long after that, `Steve` sent me another email that made him sound like a scammer:

```
[...]

We caught up with HR today at Barclays, they gave us the good news that everything has been sorted regarding the contract and you would be happy to accept under these conditions.

What is the full update from your side? Are you happy to start the on-boarding process and set a start date? I will be updating Neil on the current situation, he is keen to get things finalised.

[...]
```

Note that Barclays had not actually changed any conditions.

### Rejection

I got a few calls from `Steve` that I could not answer, and not long after that, I sent him an email:

```
I have reviewed the offer again, and it is not the best offer on the
table right now. If Barclays is refusing to negotiate, I'm sorry to say
I will have to decline it.
```

Not an hour after sending this email, I had four missed calls from an unknown number
Then I got an email from `Dominic Brown` at `TekSystems`:

```
Hi Tom,

I have tried calling a couple of times this morning – please answer your phone or call me ASAP on 0207 XXX XXXX?
Steve is now off for a couple of weeks therefore I will be looking after things.

Regards,

Dom
```

I sent `Dom` a message saying I was in a meeting, and would call him in ten minutes.
The call that followed was not fun.

`Dom` sounded very annoyed at me. He told me I was very junior, and said:

```
This offer is the best you can get.
This [55k GBP/y] is what you're worth.
You're not worth any more than this.
``` 

He told me I was rude for not picking up the phone.
I told him I was not available.
He told me I was lying, that I was available.

`Dom` sounded like a sore loser.
I thanked him for his time, and hung up.

### Timeline:

- 2017-04-26: First application suggestion
- 2017-06-08: Sign-off from HR
- 2017-06-09: Technical screening at Zurihack
- 2017-06-15: On-site interviews in London
- 2017-07-19: Offer
- 2017-08-18: Offer rejection
