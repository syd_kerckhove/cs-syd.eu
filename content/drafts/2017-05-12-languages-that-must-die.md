---
title: Languages that must die
tags: programming, languages
---

Software engineering is an ever-growing human activity.
More and more people are programming, and more code is written than ever. [CITATION NEEDED]

As programmers, we have to prioritise our work relentlessly.
As a result, given a certain problem, we often start with something that works now, and re-prioritise immediately after that.
If it then turns out that we have better things to do, the hacky solutions persists.

Later, we look back at our original solution and figure that, even though we now don't really understand the problem or the solution anymore, it must have been well thought-out at the time.
We conclude that it's best to not change anything and there is probably no better way to do things.
... and even if there were a better way, it is now too expensive to change anything.

Without challenging the status quo, we can never improve on the systems that are currently in use.
We have to be able to recognise that the current situation is not optimal, and figure out whether we can do better.

Programming is a way to talk to a computer to make it do what you want.
... but computers are incredibly literally minded.
You have to be rather precice in what you say, in order to make what you say match up with what you want to say.
Different languages make us talk in a different way.

In choosing between different languages, we have to make tradeoffs:

- How well do I know this language?
- How easy is it to write this language?
- How easy is it to be as precise as the computer needs me to be?
- How will the computer react when I make a mistake?
  Will it refuse to do anything or will it do the wrong thing?
  Will it do any damage in the process?

Over the past decades, a small number of languages have survived.
Often a programmer learns only one of those language, and no others.
Because of how few programmers there are, the first of the above tradeoffs becomes overly important if the tradeoffs are even considered.

Just because these programming languages are most popular, does not mean that they are the best tool for the job at hand.
They were an initial solution that we have since gotten stuck with.
Now we know that there are better solutions, and they are ready to be used.

These initial solutions had little regard for robustness of software.
Instead they were focussed on how fine-grained a control the programmer can have on the metal in the box.
This is why most people who use software agree: "Software often does not work."

The better tools that are available to programmers now, are more focussed on robustness.
We have found ways to structure languages in such a way that many of the things we could write wrong, are no longer available.
The computer will just not listen in that case, and that's a good thing!

Below, I have compiled a list of languages that we know have severe problems when it comes to robustness.
We already know that these languages are unsafe to use but they are still heavily used because we are stuck with them.
Some programmers even mistakenly believe that this unsafety is not a bad thing, or does not matter.

... but first I would like to present the features that languages can provide to make for a safer programming language, at negligible (or even negative) cost to the programmer.

### Easy win #1 Compilation

Compilation just means that we translate the code to another language first.
The reason that this helps with programming is that it requires the programmer to write code that the compiler can parse.
If there are any syntax errors, the compiler will complain and refuse to translate the code.
This way it is impossible to run a program with syntax errors.

### Easy win #2 Static types

Static types mean that a program 'has to make sense' with respect to the types of data being used.
This means that things that you most likely don't want to do anyway, like incrementing a function (which does not make sense), is not possible.
Static types make it impossible to run a program with type errors.

Static types without compilation is only marginaly safer than just static types by itself.
Without compilation, you will still get type errors at runtime, but they will be more descriptive than if you did not have static types.

### Easy win #3 Imutability

Immutability literally means 'the impossibility of change'.
There is a class of bugs that consist of an unexpected modification to a certain piece of data.
Baking immutability into the programming language means that these kinds of bugs can never occur.

### Easy win #4 Functional programming

Functional programming is a very overloaded term.
At the core it means that individual pieces of a program behave like functions in the mathematical sense.
The value of a function, evaluated at a certain point in its domain, depends only on the function itself, and at which point it is evaluated.
The value of a function can never depend on the current time on the clock, the current stance of the moon, or any other piece of information.

This makes it possible to reason locally in a big codebase.
Changing one part of the code can never have an impact on other, seemingly unrelated parts of the code.
An entire class of bugs is eliminated with this feature of a language.

TODO mention the alternative, why people like it and what are the tradeoffs

### Easy win #5 Enforced IO purity

It is possible for a programming language to enforce the separation of input/output from computation.
Functional programming can only get you so far, if IO purity is not enforced.
At some point the code will need to interact with the outside world, otherwise it would have no reason for existing.
Requiring the separation of IO from computation allows a programmer to be certain that any computation will not perform IO.
No unintended consequences can arise from computing a pure computation.
As such, another entire class of bugs is eliminated.

### Other wins

- Higher-order programming: allows for better composition of code, which means less specific, and therefore less, code.
- Laziness: Allows for optimisations and better composition of code, which means less code.

### Languages that must die

For the sake of safety of software, it is imperative that certain programming languages die.
Here is a list.
It is vaguely ordered by decreasing urgency.

- Python (typed, but not compiled, enforces the prejudice that conciseness comes at the cost of safety)
- Javascript (untyped, not as bad because it is sandboxed. Recently the situation has become much worse since it is also run on the server side (nodejs) or on desktop apps (Electron))
- Ruby (untyped [CITATION NEEDED], the same problems as python)
- PHP (untyped [CITATION NEEDED])
- Lisp (untyped [CITATION NEEDED])
- Clojure (untyped [CITATION NEEDED])
- Lua (untyped [CITATION NEEDED])
- Erlang (untyped [CITATION NEEDED])
- Java (typed, but the type system is very weak (and unsound) and there is no type inference)

This list is not complete.
