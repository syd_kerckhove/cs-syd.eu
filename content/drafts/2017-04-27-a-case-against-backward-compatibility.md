---
title: A case against backward compatibility
tags: backward compatibility
category: Opinion
---

The most critical code occurs on the borders of components of a system.

### Definitions

A way to agree on how the different components on a system should work together, is using a previously agreed-upon interface or protocol.
Such an interface/protocol is more commonly known as an Application Programming Interface (API).
An API is called public if there may be systems that use the API that the author of the API has no control over.

Now, systems change, so their borders change, and as a result, these interfaces/protocols must change to conform to the new reality.
A change is called backward-compatible if any outside system that uses the new version of the component does not have to change to still work.
A change is called backward-incompatible if the outside system may have to change to still work with the new component.

### The tradeoffs of maintaining backward compatibility

Suppose you built a system, but you notice that there is room for improvement, and you decide you want to upgrade the system.

There is precicely one advantage: Your system will gain the improvement that you build.
However, there may be multiple disadvantages to upgrading your system.
The disadvantages depend on a multitude of factors.
The most important factor is whether you want to maintain backward compatibility.

If you do not maintain backward compatibility, all users of your system are forced to assess whether they need to upgrade their systems in order to be compatible with your change.
If the old version of your 
