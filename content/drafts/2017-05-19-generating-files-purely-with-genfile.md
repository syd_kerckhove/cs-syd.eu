---
layout: post
title: Generating files purely with `genfile`
tags: generation
---

Generating files, when it is done, is often the last step in a pipeline of processing.
For some reason, it is mostly not done purely, which makes it hard to test.
In this post I motivate and announce the `genfile` library for generating files purely.

<div></div><!--more-->

Imagine you have have a program in which you read a number of input files, perform some computation, and then generate a number of output files.
It is imperative that the both the content of the generated files is correct, and the paths at which we generate these files is correct.

`genfile` is a tiny library that allows you to produce a data structure that describes the output as a whole, and then offers ways to render this data structure as actual files with the right content.
It uses [`path`](https://hackage.haskell.org/package/path) and [`path-io`](https://hackage.haskell.org/package/path-io) to force users to ensure that they are handling paths correctly.

Central to the library is the `GenFile` type.

``` haskell
-- | The instructions to generate a file with relativity 'rel' and contents 'str'.
--
-- 'rel' is either 'Rel' or 'Abs', both from the 'path' package.
-- 'str' can be anything, but is usually something string-like.
data GenFile rel str = GenFile (Path rel File) str
    deriving (Show, Eq)

-- | A collection of 'GenFile's
type GenDirTree rel a = [GenFile rel a]
```

Producing values of this type, that hold both the path and the intended content of the file, instead of producing paths and contents seperately, has a few benefits:

- The user can talk about generated files in a more abstract way.
- Functions that generate files can be pure up until the very last moment.
- It allows for separation of deciding what the contents of a file should be and how the actual files should be created.
- The user is required to think about how these two components relate.

Once a number of these `GenFile` values have been created, it is a completely seperate matter to decide how the actual files should be created.
`genfile` provides some options, but you are free to implement your own.

The most general option that `genfile` pesents is `renderDirTree`:

``` Haskell
-- | Render a tree of files according to a given function that decides on an
-- action to perform for every file.
--
-- This function first ensures that all required directories exist.
renderDirTree
    :: MonadIO m
    => GenDirTree Abs str
        -- ^ A tree of files
    -> (Path Abs File -> str -> m ())
        -- ^ The function that determines an action for every file.
    -> m ()
```

A more specific version, for 'String', exists as well:

``` Haskell
-- | Render a tree of files containing 'String's by writing each to their file.
renderDirTreeString
    :: MonadIO m
    => GenDirTree Abs String -> m ()
```

You can find [`genfile` on hackage](/TODO), it is ready for use.
