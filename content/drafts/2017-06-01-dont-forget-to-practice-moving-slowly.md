---
title: Don't forrget to practice moving slowly
tags: speed, practice
---

We spend an enormous amount of effort to try and work faster.
We practice typing more quickly, we practice programming with very tight time constraints (less than an hour), ... (REWRITE)
In this post I would like to remind you that there is a very real opportunity for improvement when it comes to slow, long term pieces of work.

These days, we face a lot more stuff on a day-to-day basis.
You only need to have a look at the number of emails that you get per day and you will already have a good idea of the volume of stuff that you get to deal with every day.
As a result, we have become rather good at dealing with stuff very quickly, but we have gotton worse at dealing with long-term work.

If you have a project that moves forward once every two weeks, you only need 14 projects to move forward on something exciting every day. I now have about 70 projects!
