---
layout: post
title: Another new keyboard
category: Experience
tags: keyboard, ergonomy, dvorak, frustrations, TEK
---

#### Curse Truly ergonomic!
A few weeks after [I started using the “Truly-Ergonomic Keyboard”](/posts/2014-08-31-the-switch-to-dvorak-and-an-ergonomic-keyboard.html) several keys started malfunctioning.
Some keys didn't register any keystrokes at times and other keys registered the same stroke twice.
To give you an idea of what that looks like, I will type the following paragraph with the broken keyboard.

I sent a reqquestt for customer's service to te compannyyy.
I gooooot an autooomatic respppponse: "Dear valued customer..." but never did I get an answer from a uman.
I sent a few of tese requests.
Eventually I sent a request to returnnn te keyyyboard (and have it fixed).
Still no answwwewer.
Tis time I didn't even get an automatic respponse!

<div></div><!--more-->

I gave up on this company.
Needless to say, I don't recommend the keyboard to anyone.
It's my own fault for confiding in such a young company, I guess.

#### Hello Kinesis advantage
Even though Truly-Ergonomic has failed me, I still like the comfort of an ergonomical keyboard as well as the mechanical keys.
I bought a new ergonomical mechanical keyboard: the Kinesis advantage.

![](/assets/another-new-keyboard/advantage-black.jpg)

Of course I'm making a custom layout based on programmers dvorak.
Let's hope that the kinesis advantage lasts longer than 6 months.

#### Typing speed
For those of you who wonder how fast i've gotten on a dvorak layout (I still plan to reach a speed of 125wpm some day!):
I'd reached a speed of 60wpm before the keyboard broke.
Of course, with a broken keyboard, typing tests don't give an accurate result.
After only one day with the advantage, my speed had reached 70wpm.
The new keyboard promises to be amazing!

