---
layout: post
title: Erasmus, the good, the bad and the ugly
category: Experience
tags: Erasmus,exchange
---

I am currently on an Erasmus exchange, a trip I anticipated for years.
I am somewhat disappointed and I want to present future generations of students with my story so that they may make an informed decision about a year abroad.


<div></div><!--more-->

### Anticipation

An Erasmus exchange had been on my mind ever since I started studying.
I would get to go to another country for three to nine months and study at another university.
I would get to expand my world view and meet amazing new people.

Less than a year before I'd go, I went to a information lecture where the Erasmus coordinators of my university explained the entire process.
On the list of possible destinations, I looked for the best university.
I figured I'd be able to get a great education for a few months, after which I'd have to go back as planned.

I intended to be a good student and not just go on the exchange to party abroad.
I got my parents to agree to pay for an exchange in one of the most expensive cities in the world.

After a suspenseful nomination procedure from my university that would decide who would get to go, I got to wait some more until my application was reviewed by ETH.
More than six months after I'd gotten the list of possible destinations, I finally got confirmation that I was clear to go to Zurich, Switzerland.

I had spent more than 100 hours doing research, preparing the nomination application, preparing the acceptance application, preparing to move to Switzerland.
That is not to mention the dozens of hours we spent looking for an apartment before we went, [the time I was stranded in a strange country without an ID card or money and lost 4000 Euro's to a scam](http://cs-syd.eu/posts/2015-09-20-postmortem-rental-scam.html) or days we spent finding another apartment once we got to Switzerland.

After spending a month in stress with a family that kindheartedly took us in, I found myself in a nice apartment by night, attending a good school by day.

### The good

ETH is a better university than KU Leuven and not only on paper.
Of course I cannot say anything about the research that's being done at ETH but the university is better organized and the systems are more sane towards students.

Students have a much freer choice about which courses they take, there are more specializations and they are more varied.
There are world experts teaching the courses.
I even got to take a 'Software Verification' class from Bertrand Meyer himself!

At ETH only about one third of the applicants gets in whereas at KU Leuven everyone gets accepted automatically.
Not only does this mean that the average student is more competent and thus more fun to work with in group projects, the students are generally much more motivated and accomplished.

### The bad

#### Courses and workload
Even though ETH is a better university toward it students, I could only partially enjoy that.

One of the rules of the exchange was that every course that I took at ETH had to exist at KU Leuven as well or else I could not take it.
I didn't not know this beforehand.
It defeats the entire purpose of an Erasmus exchange.
All the courses that seemed interesting at ETH now weren't available to me.

Another problem is the workload.
It is hard enough to move to another country, find a place to live, get a few dozen hours of paperwork done.
Then you still have to start studying.
At my home university, taking 30 credits per semester is already an equivalent workload to a full-time job, but it is expected and a student tries by default.
At ETH, taking 30 credits per semester is almost unheard of.
Most computer science students take less than 25 credits per semester because it's just too high a workload.

Finishing my masters degree in the expected two years requires me to take at least 30 credits on average each semester.
I would have to work extraordinarily hard to get through this year just because I am on exchange.

As if that wasn't bad enough, because I was restricted to courses that exist at my home university, I've had to combine courses that no sane student would try to combine in one semester.
As a result, I've had more than 10 group projects this semester.
Sleepless nights were ensured from the start.

To get the more than 30 credits that I committed to, I had to overcome a workload equivalent to almost two full-time jobs.

Hold on, because the worst is yet to come.
At the start of the year I got a letter from the student exchange office.
It said that if I failed to complete the courses I had set out to do, I would have to pay back my entire scholarship.

No stress...

#### Living costs

Now it is not as if I would have been able to party in Zürich all year long otherwise.
Zürich is in the top three most expensive cities in the world (even the most expensive, depending on the source).
With what we paid for an apartment in Zürich, I could have lived in a 400m² villa in Leuven instead.

"But you got a scholarship!" you may be thinking right now.
That is true, but even without the strings attached, that would've gotten me through less than two months of rent and nothing else, instead of ten months of studying.
Even with a part-time job as a teaching assistant, I could only pay for food.
A big thank-you to my parents, without whom this exchange would certainly not have been possible.

### The ugly

An Erasmus exchange was described as a life changing event.
I was told you could study new subjects that would never by covered at my home university.
They obviously weren't acurately describing what was going to happen.

While the exchange has been amazing from a personal perspective, it has been no better than staying at home from an academic perspective.

### Conclusion

If you were planning to go on an Erasmus exchange, I hope you consider that maybe you haven't been told everything.
If you would like to study abroad, go and study abroad, but don't go on an Erasmus exchange.

If you really want to go on an Erasmus exchange, don't go to study.
Find a nice, cheap, warm country with a university where you can pass courses easily without much work and take a year off, partying at the beach.
