---
layout: page
title: Contact
header: Contact
---

We have a plethora of communication media at our disposal.
This makes it difficult to agree on protocols.
Here is mine.

<div></div><!--more-->

I purposely don't have an account on most social network sites.
There is only one way to reach me reliably and that is *email*.

Before you do contact me, there are some things you need to know:

It is important to note that this concerns communcation addressed to me.
I will enforce certain standards on those communications.
Feel free to make up your own for yours. 
Here are my standards:

- If you expect me to address you formally, you will address me formally.
- If I don't know you, we are not on a first name basis.
- 80 Collumns per line
- If I'm not the only one that the email is adressed to, I will regard it as though I was in CC.
- If you mention a time of day, use 24h time.
- If you mention timestamps, always include the timezone.
- If you put me in CC, I will not respond to the email. I will only read it.
- One sentence per line.
- Only one subject per email.
- Plaintext, not HTML-only.
- Reply inline.
- I will read an email within five working days.

Even though I have these standards, I am not usually unpleasant.
Feel free to contact me if sincere.
I won't bite.

Here is the address:
<div class="address">
  <span class="fn email">
    syd.kerckhove<span style="display:none">muhaha</span>@gmail.com
  </span>
</div>
