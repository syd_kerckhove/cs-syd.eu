---
title: Cv and Timeline
---

Over time I have realised that having only one CV is quite limiting.
Different people have different expectations when it comes to a CV, so I made multiple CV documents:

- <a href="/cv/for-recruiters.pdf">A CV for recruiters</a>
- <a href="/cv/for-engineers.pdf">A CV for engineers</a>
- <a href="/cv/timeline.html">A Timeline of what I have been up to in the last few years</a>
- <a href="/cv/failcv.pdf">A CV of Failures</a>
- <a href="/cv/cv.pdf">A regular CV</a>
